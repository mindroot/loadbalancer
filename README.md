---

#Loadbalancer for the iota-ecosystem

---

###Informations

---


The `docker-compose.yml` builds an image from the `Dockerfile` and starts a container. The container contains a nginx-instance. \
Within the `docker-compose.yml`, two volumes are mounted to the container:

---

| Mime-Type   | Host         | Container             |
|:-----------:|:------------:|:---------------------:|
| text/plain        | ./nginx.conf | /etc/nginx/nginx.conf |
| inode/directory   | ./conf.d/    | /etc/nginx/conf.d/    |

---

These files are required to redirect the users to the different ecosystems, defined in the `nginx.conf`.
```
upstream ecosystem {
    # comment out "ip_hash;" for testing purposes
    ip_hash;
    server 127.0.0.1:8081 weight=2; #GCP - ecosystem-dev.mind-me.de
    server 127.0.0.1:8082 weight=8; #Heroku - ecosystem.iota.org
}
```
The parameter `weight` is used for the **percentage load balancing**.

---

###Installing

---

```
git clone https://gitlab.com/mindroot/loadbalancer
cd loadbalancer && docker-compose up --build
```

The loadbalancer starts up on port 5050, accessible over http://localhost:5050/
